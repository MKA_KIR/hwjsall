# step-project-cards
### Молчанов Кирилл, FE-13 - сделано:
- Дизайн и HTML разметка страницы;
- Класс Component как родительский для всех классов;
- Методы setState, reconciliation, createElement класса Component
- Класс Visit (описывающий общие для всех визитов к любому врачу поля и методы);
- Дочерние классы VisitDentist, VisitCardiologist, VisitTherapist;
- Класс Form (для всех форм);
- Класс VisitForm - общий для всех форм которые работают с визитами
- Классы VisitCardioForm, VisitDentistForm, VisitTherapistForm;
- отдельные классы (компоненты) для полей формы (Input, Select, Textarea);
- GET запрос на получение всех ранее добавленных карточек при загрузке страници;
- DELETE запрос на удаление карточки;
- PUT запрос на обновление карточки;



